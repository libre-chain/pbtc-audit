# PBTC Audit

Public repository for BTC audit of PBTC decentralized custody on PNetwork.

## Don't Trust, Verify 

The amount of PBTC circulating is here in the 'supply' field available here or from any [validator API nodes](https://libre.eosio.online/endpoints) ([you can also run your own API to check yourself](https://gitlab.com/libre-chain/libre-chain-nodes)):

https://lb.libre.org/v1/chain/get_currency_stats?code=btc.ptokens&symbol=pbtc

**Note about precision:**
* PBTC is denominated down to "0.1 SATS" so the precision of the PBTC token is 9 decimals. 
* Bitcoin is 8 decimals. 
* Lightning Network goes to BTC msats (0.001 SATS) which is 11 decimals.

The BTC addresses containing the BTC controlled by PNetwork can be found here in this repo:

[./public/index.json](./public/index.json)

Take any of those addresses and look them up in a block explorer to verify.

The total amount of SATS in each should match the JSON.

If you have any questions or issues with PNetwork pegged assets, please visit them on [Telegram](https://t.me/pNetworkdefiachat). Libre Chain is not affiliated with PNetwork. 